# KSE-1309.2949
A Mathematica notebook solving the Killing spinor equations of 1309.2949

## Introduction

This Mathematica notebook follows [1](#1512.08356) and solves the Killing spinor equations of the localised solution found in [2](#1309.2949).

This should serve as an example of how to perform operations on, and solve, Killing spinor equations using Mathematica.

For more information see notebook.

## References

<a name="1512.08356">1</a>: 
Fermionic T-duality in massive type IIA supergravity on AdS\_{10-k} x M\_k
Ilya Bakhmatov
[1512.08356](https://arxiv.org/abs/1512.08356)

<a name="1309.2949">2</a>:
All AdS\_7 solutions of type II supergravity
Fabio Apruzzi, Marco Fazzi, Dario Rosa, Alessandro Tomasiello
[1309.2949](https://arxiv.org/abs/1309.2949)


